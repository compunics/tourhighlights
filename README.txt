The plugin "TourHighlights" has been tested with V2.5.3 of PTP on Linux.

Plugin description:
The plugin enables automatic changes of panos and movements between individual hotspots.
You can select if the autotour covers all panos, only the first pano or an list of selected panoids.

Usage:
Assign a Hotspot-style called 'TourHighlightsSpot' to the hotspots that will form the automatic tour.
The hotspot-image can be an invisible '1x1'-pixel transparent graphic (included in the plugin directory).

Using the hotspot description, you can assign individual move properties for each hotspot as follows:
Field of view (or zoom-factor in°): values between 1 and 179
Speed: movement in degrees per second (default: 120)
Wait: time in seconds before the next movement
Give the parameters on a single line, separated by a semicolon ';', example:
70;100;4

You can select an image to be presented at startup, so that the automatic tour starts when the user clicks on that image.
Starting with Version 1.1 you can also select an endless-loop tour, in that case make sure to not use a startup-image, as that would require manual intervention to let the presentation mode start.

Starting an automatic tour presentation:
You can let the presentation start automatically on Tourstart, if you don't want that behaviour
you can assign the '[Plugin] TourHighlights > Start the TourHighlights' - action to a button or hotspot,
or you can assign the startup-action to any other event using the additional 'Call Actions On Tour-Events'-plugin (CAOTE).

Interruption/termination:
You can select to let the user interrupt/terminate the automatic presentation mode, but there is no function
to resume presentation mode from that point. Hint: just moving the mouse will not cause an interruption,
but clicking and/or moving the pano will do.
